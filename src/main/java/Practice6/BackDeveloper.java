package Practice6;

public interface BackDeveloper extends Developer {
    void writeBack();
}
