package Practice6;

public class Main {
    public static void main(String[] args){

        Company company = new Company();
        company.addEmployee(new JavaDeveloper("Abai"));
        company.addEmployee(new JsDeveloper("Marat"));
        company.addEmployee(new FullDeveloper("Dias"));
        company.startWork();
    }
}
