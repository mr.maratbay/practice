package Practice6;

public  abstract  class JsDeveloper extends Worker implements FrontDeveloper,Tea {
    public JsDeveloper(string name) {
        Super(name);
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
        develop();
        makeTea();
    }

    @Override
    public void writeFront() {
        System.out.println("My name is " + getName() + ",It is so hard");
    }

    @Override
    public void makeTea() {
        System.out.println("Here is your Tea.Please!");
    }
}
