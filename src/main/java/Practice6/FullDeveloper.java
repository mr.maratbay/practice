package Practice6;
public abstract  class  FullDeveloper extends Worker implements BackDeveloper, FrontDeveloper {

    public FullDeveloper(String name) {
        super(name);
    }

    @Override
    public void writeBack() {
        System.out.println("My name is " + getName() + ", learning!");
    }

    @Override
    public void writeFront() {
        System.out.println("My name is " + getName() + ", Learningg");


        @Override
        public void develop () {
            writeFront();
            writeBack();
        }

        @Override
        public void work () {
            develop();
        }
    }
}
