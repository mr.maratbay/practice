package Practice6;

public abstract class JavaDeveloper extends worker implements BackDeveloper {
    public JavaDeveloper(String name){
        super(name);
    }

    @Override
    public void work() {
        develop();
    }

    @Override
    public void writeBack() {
        System.out.println("My name is " + getName() + ", Never give up to learn");
    }

    @Override
    public void develop() {
        writeBack();
    }
}
